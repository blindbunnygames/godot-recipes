extends Node2D

export(float) var speed = 10.0

func _ready():
	pass

const action_to_move = {
	"ui_left": Vector2(-1,0),
	"ui_right": Vector2(1,0),
	"ui_up": Vector2(0,-1),
	"ui_down": Vector2(0,1)
}

func _input(event):
	if event is InputEventKey:
		for action in action_to_move.keys():
			if event.is_action(action):
				position += speed*action_to_move[action]