extends Node

export(NodePath) var viewport_node = "../Viewport"

onready var target = get_node(viewport_node).get_child(0)

func _ready():
	set_process_input(true)
	pass

func _input(event):	
	target._input(event)
