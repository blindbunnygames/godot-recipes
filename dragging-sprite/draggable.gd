extends Sprite

var dragging = false
var last_position

func _ready():
	$Area2D.connect("input_event",self,"area_event")
	last_position = global_position
	
func area_event(viewport,event,shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			dragging = event.pressed

func _process(delta):
	if dragging:		
		global_position = get_global_mouse_position()
		# rotating sprite to point in direction
		var position_delta = global_position - last_position
		
		# wait until we drag some distance to prefent spinning in place
		if position_delta.length() > 5:
			last_position = global_position
			
			# use lerp to smooth rotation over distance
			rotation = lerp(rotation,position_delta.angle(),0.2)