When I answered questions on Godot [Q&A](https://godotengine.org/qa/questions) I tried to test if my answer was working by creating simple demo project. So I thought that it may be handy to save all those demos in one repository so others can use them for learning. 

For now it contains only few demos but in future they will be more.