extends Node2D

var character_features = {
	"nick": "string",
	"gender": ["male","female"],
	"age": {
		"range": [10,100]
	},
	"race": ["human","elf","dwarf","undead politician"]
}

func generate_range(array_range):
	var ui_node = SpinBox.new()
	ui_node.min_value = array_range[0]
	ui_node.max_value = array_range[1]
	return ui_node
	
func generate_selection(items):
	var ui_node = OptionButton.new()
	for item in items:
		ui_node.add_item(item)
	return ui_node

func generate_form(features):
	var result = VBoxContainer.new()
	for k in features.keys():
		var row = HBoxContainer.new()
		result.add_child(row)
		var label = Label.new()
		label.text = k
		row.add_child(label)
		var v = features[k]
		var control_node;
		match typeof(v):
			TYPE_DICTIONARY: control_node = generate_range(v["range"])
			TYPE_ARRAY: control_node = generate_selection(v)
			TYPE_STRING: control_node = LineEdit.new()
			_ : print("unsupported type in " + k)
		if control_node != null:
			print(control_node)
			row.add_child(control_node)
	return result

var form
var description

func get_value(item):
	if item is SpinBox:
		return item.value
	else: if item is LineEdit:
		return item.text
	else: if item is OptionButton:
		var id = item.get_selected_id()
		var idx = item.get_popup().get_item_index(id)
		return item.get_item_text(idx)

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	form = generate_form(character_features)
	var button = Button.new()
	button.text = "OK"
	button.connect("pressed",self,"process_character_form")
	form.add_child(button)
	$VSplitContainer/CenterContainer/VBoxContainer.add_child(form)
	pass

func parse_character_form(form):
	var result = {}
	for row in form.get_children():
		var label = row.get_child(0)
		if label:
			var key = label.text
			var value_item = row.get_child(1)
			if value_item:
				var value = get_value(value_item)
				result[key] = value
	return result
	

func generate_welcome_message(character):
	var message = "You were born as a " + character.gender
	message += " " + character.race + "."
	message += " You were given name " + character.nick + "."
	message += " After many years you've completed your training and now at age of " + var2str(character.age) + " you are ready for adventures"
	return message

func process_character_form():
	var character = parse_character_form(form)
	$VSplitContainer/RichTextLabel.text = generate_welcome_message(character)
	
