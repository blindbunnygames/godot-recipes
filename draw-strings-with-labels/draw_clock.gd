extends Control

export(PackedScene) var Text # this is label with our desired font applyied
export(float) var radius = 20

func _ready():
	var position = Vector2(-radius,0)
	for i in range(1,13):
		var label = Text.instance()
		label.text = var2str(i)
		var angle_in_radians = -(i - 1) / 12.0 * 2 * PI
		var rotated_position = position.rotated(angle_in_radians)
		label.margin_top = rotated_position.x
		label.margin_left = rotated_position.y
		add_child(label)