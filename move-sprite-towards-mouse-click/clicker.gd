extends Node2D

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		$Tween.interpolate_property($sprite, "global_position",$sprite.global_position, get_global_mouse_position(), 1,Tween.TRANS_LINEAR,Tween.EASE_IN)
		$Tween.start()
